import bcrypt from 'bcrypt-nodejs'
import * as jwt from '../services/jwt'
import User from '../models/user'

export function signup(req, res) {
    const user = new User()

    if (req.body.password) {
        bcrypt.hash(req.body.password, null, null, (err, hash) => {
            user.password = hash
        })
    } else
        res.status(200).send({ message: `Enter password` })

    user.email = req.body.email
    user.username = req.body.username
    user.firstname = req.body.firstname
    user.lastname = req.body.lastname
    user.category = req.body.category
    user.signupDate = new Date()
    user.birthday = req.body.birthday
    user.experience = 0
    user.headerFileName = null
    user.avatarFileName = null
    user.statusText = null
    user.numberOfVisits = 0
    user.numberOfVictories = 0
    user.numberOfDefeats = 0
    user.linkToFacebookProfile = null
    user.linkToTwitterProfile = null
    user.linkToDeviantartProfile = null
    user.toggleFacebook = true
    user.toggleTwitter = true
    user.toggleDeviantart = true
    user.isActive = true
    user.role = req.body.role

    user.save((err) => {
        if (err)
            res.status(500).send({ message: `user creation error: ${err}` })
        
        res.status(201).send({ token: jwt.createToken(user) })
    })
}

export function login(req, res) {
    const params = req.body
    const email = params['email']
    const password = params['password']

    User.findOne({ email: email.toLowerCase().trim() }, (err, user) => {
        if (err)
            res.status(500).send({ message: `Server error: ${err}` })
        else {
            if (!user)
                res.status(404).send({ message: `email doesn't exist` })
            else {
                bcrypt.compare(password, user.password, (err, check) => {
                    if (check) {
                        res.status(200).send({
                            token: jwt.createToken(user),
                            user: user
                        })
                    } else {
                        res.status(404).send({ message: `Could not log in: ${err}` })
                    }
                })
            }
        }
    })
}

export function getUsers(req, res) {
    User.find().exec((err, users) => {
        if (err) 
            res.status(500).send({ message: `request error: ${err}` })
        else {
            if (!users) 
                res.status(404).send({ message: `there are no users` })
            else 
                res.status(200).send({ users: users })
        }
    })
}
import mongoose from 'mongoose'
const Schema = mongoose.Schema

const UserSchema = new Schema({
    email: { type: String, unique: true, lowercase: true, required: true },
    password: { type: String, select: true, required: true },
    username: { type: String, unique: true, lowercase: true, required: true },
    firstname: String,
    lastname: String,
    category: { type: String, enum: ['illustration', 'photography', 'watcher'] },
    signupDate: { type: Date, default: Date.now() },
    birthday: Date,
    experience: Number,
    headerFileName: String,
    avatarFileName: String,
    statusText: String,
    numberOfVisits: Number,
    numberOfVictories: Number,
    numberOfDefeats: Number,
    linkToFacebookProfile: String,
    linkToTwitterProfile: String,
    linkToDeviantartProfile: String,
    toggleFacebook: Boolean,
    toggleTwitter: Boolean,
    toggleDeviantart: Boolean,
    isActive: Boolean,
    role: String
})

export default mongoose.model('User', UserSchema)